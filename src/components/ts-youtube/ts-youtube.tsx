// https://developers.google.com/youtube/v3/docs/videos

// Web Component inspired by:
// https://webdesign.tutsplus.com/tutorials/how-to-lazy-load-embedded-youtube-videos--cms-26743
// Todo:
//   – optimize code
//   – check metrics for loading; a) thumbnails, end b) videos

import { Component, Prop, h } from "@stencil/core";

@Component({
  tag: "ts-youtube",
  styleUrl: "ts-youtube.scss",
  shadow: true
})
export class TsYoutube {
  @Prop() youtubeid: string;
  @Prop() width: number;
  @Prop() title: string;
  @Prop() description: string;
  @Prop() level: string;
  @Prop() videoPlayer: string;

  calcHeight = () => {
    let cWidth = this.width;
    // 16/9 proportions
    let cHeight = cWidth * 0.56;
    return cHeight.toFixed(0);
  };

  getImage = () => {
    let id = this.youtubeid;
    let src = "https://img.youtube.com/vi/" + id + "/hqdefault.jpg";
    return src;
  };

  playVideo() {
    let id = this.youtubeid;
    let iframe =
      `<iframe width="560" height="315" src="https://www.youtube.com/embed/` +
      id +
      `" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>`;
    this.videoPlayer = iframe;
  }

  render() {
    return (
      <div class="ts-youtube" innerHTML={this.videoPlayer}>
        <button class="play-button" onClick={() => this.playVideo()}></button>
        <img src={this.getImage()} />
        <p class="meta">
          <span class="heading">{this.title}</span>
          <span class="description">{this.description}</span>
          <span class="level">{this.level}</span>
        </p>
      </div>
    );
  }
}
