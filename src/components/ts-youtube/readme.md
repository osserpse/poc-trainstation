# ts-youtube



<!-- Auto Generated Below -->


## Properties

| Property      | Attribute      | Description | Type     | Default     |
| ------------- | -------------- | ----------- | -------- | ----------- |
| `description` | `description`  |             | `string` | `undefined` |
| `level`       | `level`        |             | `string` | `undefined` |
| `title`       | `title`        |             | `string` | `undefined` |
| `videoPlayer` | `video-player` |             | `string` | `undefined` |
| `width`       | `width`        |             | `number` | `undefined` |
| `youtubeid`   | `youtubeid`    |             | `string` | `undefined` |


----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
