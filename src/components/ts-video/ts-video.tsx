import { Component, Prop, h } from "@stencil/core";

@Component({
  tag: "ts-video",
  styleUrl: "ts-video.scss",
  shadow: true
})
export class TsVideo {
  @Prop() youtubeid: string;
  @Prop() width: number;
  @Prop() heading: string;
  @Prop() description: string;
  @Prop() level: string;
  @Prop() course: string;
  @Prop() nextvideo: string;

  calcHeight = () => {
    let cWidth = this.width;
    // 16/9 proportions
    let cHeight = cWidth * 0.56;
    return cHeight.toFixed(0);
  };

  getUrl = () => {
    let id = this.youtubeid;
    let url = "https://www.youtube.com/embed/" + id;
    return url;
  };

  render() {
    return (
      <div class="ts-video">
        <iframe
          width={this.width}
          height={this.calcHeight()}
          src={this.getUrl()}
          frameborder="0"
          // allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
          // allowfullscreen
        ></iframe>
        <p class="meta">
          <span class="heading">{this.heading}</span>
          <span class="description">{this.description}</span>
          <span class="level">{this.level}</span>
        </p>
      </div>
    );
  }
}
