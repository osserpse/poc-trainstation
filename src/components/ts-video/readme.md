# ts-video



<!-- Auto Generated Below -->


## Properties

| Property      | Attribute     | Description | Type     | Default     |
| ------------- | ------------- | ----------- | -------- | ----------- |
| `course`      | `course`      |             | `string` | `undefined` |
| `description` | `description` |             | `string` | `undefined` |
| `heading`     | `heading`     |             | `string` | `undefined` |
| `level`       | `level`       |             | `string` | `undefined` |
| `nextvideo`   | `nextvideo`   |             | `string` | `undefined` |
| `width`       | `width`       |             | `number` | `undefined` |
| `youtubeid`   | `youtubeid`   |             | `string` | `undefined` |


----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
